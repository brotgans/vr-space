let vrMode = (typeof renderhooks === 'object');
//var THREE = require('three');
import * as THREE from 'three';
import { generateGrid, generateTube } from './modules/helpers/grid';
import $ from 'jquery';
import _ from 'underscore-node';

// ///////////////
// SETUP
// ///////////////
const scene = new THREE.Scene();
//let camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
let camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 10000);
let renderer = new THREE.WebGLRenderer({alpha: true }); // antialias: true, alpha: true
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.setClearColor( "rgb(10, 10, 10)", 1 );

let groupOfGridsDown = [];
let groupOfGridsUp = [];
let groupOfOrangeDots = [];
let groupOfBlueDots = [];
//let groupOfTubes = [];

let cameraYTarget = 0;
let speedTarget = 0.9;
let speed = 0.9;


$("body").keypress(function( event ) {
	console.log(event.which);
	event.preventDefault();
	// if (event.which == 13) {
	//
	// }

	if (event.which == 49) {
		event.preventDefault();

		cameraYTarget = 0;
		speedTarget = 0.9;
	}

	if (event.which == 32) {
		event.preventDefault();

		cameraYTarget = -110;
		speedTarget = 3.1;
	}
});


// ///////////////
// GRID
// ///////////////

for(let i = 0; i < 10; i++) {
	let line = generateTube();
	line.position.z = -i*100;
	line.position.y = 0;
	line.rotation.z = i*((Math.PI*2)/10.0);
	//line.position.x = i*10;
	//scene.add(line);
	//groupOfTubes.push(line);
}


for(let i = 0; i < 10; i++) {
	let line = generateGrid();
	line.position.z = -i*100;
	line.position.y = -120;
	//line.position.x = i*10;
	scene.add(line);
	groupOfGridsDown.push(line);
}


// ///////////////
// CUBE
// ///////////////


for(let i = 0; i < 100; i++) {
	let geometry = new THREE.SphereGeometry(5.5, 20, 16, 0, Math.PI * 2, 0, Math.PI * 2);
	let material = new THREE.MeshBasicMaterial({color: "rgb(25, 100, 255)"});
	const cube = new THREE.Mesh(geometry, material);
	cube.position.x = -500+Math.random()*1000;
	cube.position.y = -120+Math.random()*120;
	cube.position.z = -i*100;
	scene.add(cube);
	groupOfBlueDots.push(cube);
}


for(let i = 0; i < 10; i++) {
	let geometry = new THREE.SphereGeometry(2.0, 20, 16, 0, Math.PI * 2, 0, Math.PI * 2);
	let material = new THREE.MeshBasicMaterial({color: "rgb(255, 100, 0)"});
	const cube = new THREE.Mesh(geometry, material);
	cube.position.x =  -50+Math.random()*100;
	cube.position.y =  -120;
	cube.position.z = -i*100;
	scene.add(cube);
	groupOfOrangeDots.push(cube);
}

// ///////////////
// UPDATE
// ///////////////

camera.position.z = 0;
let frameCount = 0;




function animate(frame) {

	camera.position.z -= speed;
	speed = speedTarget*0.1 + speed*0.9;
	updateGroupOfGrids();

	frameCount++;

	camera.position.y = cameraYTarget*0.1+ camera.position.y*0.9;
	


	$(".number").html(Math.round(camera.position.z));

	if (vrMode) camera.quaternion.copy(
		frame.orientation
	);

	for(let i in groupOfGridsUp) {
		let dif = camera.position.z - groupOfGridsUp[i].position.z;
		groupOfGridsUp[i].material.opacity = map(dif, -50, 700, 1.0, 0.0 );
	}

	for(let i in groupOfGridsDown) {
		let dif = camera.position.z - groupOfGridsDown[i].position.z;
		groupOfGridsDown[i].material.opacity = map(dif, -50, 700, 0.4, 0.0 );
	}

	// for(let i in groupOfTubes) {
	// 	let dif = camera.position.z - groupOfTubes[i].position.z;
	// 	groupOfTubes[i].material.opacity = map(dif, -50, 700, 0.2, 0.0 );
	// }

}

function map(value, low1, high1, low2, high2) {
	return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

function updateGroupOfGrids() {

	groupOfGridsDown = _.sortBy(groupOfGridsDown, function(o) { return -o.position.z; });

	for(let i in groupOfGridsDown) {
		let dif = camera.position.z - groupOfGridsDown[i].position.z;
		if (camera.position.z - groupOfGridsDown[i].position.z < -50) {
			groupOfGridsDown[0].position.z -= (100 * 10);
		}
	}

	groupOfGridsUp = _.sortBy(groupOfGridsUp, function(o) { return -o.position.z; })

	for(let i in groupOfGridsUp) {
		let dif = camera.position.z - groupOfGridsUp[i].position.z;
		if (camera.position.z - groupOfGridsUp[i].position.z < -50) {
			groupOfGridsUp[0].position.z -= (100 * 10);
		}
	}

	groupOfOrangeDots = _.sortBy(groupOfOrangeDots, function(o) { return -o.position.z; });

	for(let i in groupOfOrangeDots) {
		let dif = camera.position.z - groupOfOrangeDots[i].position.z;
		if (camera.position.z - groupOfOrangeDots[i].position.z < -50) {
			groupOfOrangeDots[0].position.z -= (100 * 10);
		}
	}

	groupOfBlueDots = _.sortBy(groupOfBlueDots, function(o) { return -o.position.z; })

	for(let i in groupOfBlueDots) {
		let dif = camera.position.z - groupOfBlueDots[i].position.z;
		if (camera.position.z - groupOfBlueDots[i].position.z < -50) {
			groupOfBlueDots[0].position.z -= (100 * 10);
		}
	}

	// groupOfTubes = _.sortBy(groupOfTubes, function(o) { return -o.position.z; })
	//
	// for(let i in groupOfTubes) {
	// 	let dif = camera.position.z - groupOfTubes[i].position.z;
	// 	if (camera.position.z - groupOfTubes[i].position.z < -50) {
	// 		groupOfTubes[0].position.z -= (100 * 10);
	// 	}
	// }

}

function render(renderTime, vrInput) {
	requestAnimationFrame( render );
	if (typeof(vrInput) !== 'object' || vrInput.eye === 0) {
		animate(vrInput);
	}
	renderer.render(scene, camera);
}

if (!vrMode) {
	var container = document.createElement( 'div' );
	document.body.appendChild( container );
	container.appendChild( renderer.domElement );
}

render();

// trash code

// for(let i = 0; i < 10; i++) {
// 	let line = generateGrid();
// 	line.position.z = -i*100;
// 	line.position.y = 120;
// 	//line.position.x = -i*10;
// 	scene.add(line);
// 	groupOfGridsUp.push(line);
// }

// for(let i = 0; i < 10; i++) {
// 	let line = generateGrid();
// 	line.position.z = -i*100;
// 	line.position.y = 20;
// 	scene.add(line);
// 	groupOfGridsDown.push(line);
// }
