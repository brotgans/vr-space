import * as THREE from 'three'

export function generateGrid() {

	let geometry = new THREE.Geometry();
	const grid_amount = 10;
	const dist = 5;

	let i;
	for(i = 0; i <= grid_amount; i++) {
		geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist), 0,  -(grid_amount*dist)+(i*dist*2) ) );
		geometry.vertices.push( new THREE.Vector3( (grid_amount*dist),  0, -(grid_amount*dist)+(i*dist*2) ) );
	}
	for(i = 0; i <= grid_amount; i++) {
		geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist)+(i*dist*2), 0, -(grid_amount*dist) ) );
		geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist)+(i*dist*2), 0, (grid_amount*dist) ) );
	}

	let material = new THREE.LineBasicMaterial({  color: "rgb(255, 255, 255)",  opacity: 0.1, transparent: true});
	let line = new THREE.Line(geometry, material, THREE.LinePieces);

	return line;
}

export function generateTube() {

	let geometry = new THREE.Geometry();
	const grid_amount = 2;
	const dist = 1;

	let i;
	for(i = 0; i <= grid_amount; i++) {
		geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist),  -(grid_amount*dist)+(i*dist*2), 0 ) );
		geometry.vertices.push( new THREE.Vector3( (grid_amount*dist), -(grid_amount*dist)+(i*dist*2), 0 ) );
	}
	for(i = 0; i <= grid_amount; i++) {
		geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist)+(i*dist*2), -(grid_amount*dist), 0 ) );
		geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist)+(i*dist*2), (grid_amount*dist), 0 ) );
	}

	let material = new THREE.LineBasicMaterial({  color: "rgb(255, 255, 255)",  opacity: 0.1, transparent: true});
	let line = new THREE.Line(geometry, material, THREE.LinePieces);

	return line;
}

