/*
 * Sample RenderHooks Application
 */

var vrMode = (typeof renderhooks === 'object');
if (typeof THREE === 'undefined') { var THREE = require('three'); }
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 90, window.innerWidth / window.innerHeight, 0.1, 1000 );
var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.setClearColor( 0x222222, 1 );

var light = new THREE.DirectionalLight( "rgb(255, 255, 255)" );
light.position.set( 10, 1, 1 ).normalize();

var geometry = new THREE.Geometry();

var grid_amount = 10;
var dist = 15;

 for(var i = 0; i <= grid_amount; i++) {
      geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist), 0, -(grid_amount*dist)+(i*dist*2) ) );
      geometry.vertices.push( new THREE.Vector3( (grid_amount*dist), 0, -(grid_amount*dist)+(i*dist*2) ) );
 }
 for(var i = 0; i <= grid_amount; i++) {
      geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist)+(i*dist*2), 0, -(grid_amount*dist) ) );
      geometry.vertices.push( new THREE.Vector3( -(grid_amount*dist)+(i*dist*2), 0, (grid_amount*dist) ) );
 }


var material = new THREE.LineBasicMaterial({ color: "rgb(255, 255, 255)", opacity: 0.1, transparent: true });
var line = new THREE.Line( geometry, material, THREE.LinePieces);

line.position.y = -10;
scene.add( line );

var geometry = new THREE.SphereGeometry(3, 10, 20, 0, Math.PI * 2, 0, Math.PI * 2);
var material = new THREE.MeshBasicMaterial( { color: "rgb(255, 100, 0)"});
var cube = new THREE.Mesh(geometry, material);
cube.position.x = -10;
cube.position.z = 0;
scene.add(cube);

scene.add(cube);
scene.add(cube);
scene.add(cube);
scene.add(cube);


var meshArray = [];



var index = 0;

var loader = new THREE.TextureLoader();
loader.load('panotje.png', function ( texture ) {


	for(var y = -1; y <= 1; y++) {
		for (var i = 0; i < 20; i++) {



			var geometry = new THREE.SphereGeometry(0.5, 32, 32);
			var material = new THREE.MeshPhongMaterial();
			material.map = texture;

			var earthMesh = new THREE.Mesh(geometry, material);
			earthMesh.position.z = Math.cos(((Math.PI*0.05*y)) + i * Math.PI * 2 / 20.0) * 4.0;
			earthMesh.position.x = Math.sin(((Math.PI*0.05*y)) + i * Math.PI * 2 / 20.0) * 4.0;
			earthMesh.position.y = y*0.8;
			meshArray[index] = earthMesh;
			index++;

			scene.add(earthMesh);
		}
	}



});

scene.add( light );


cube.position.z = -2;
camera.position.z = 0;

var frameCount = 0.0;

function animate(frame) {

  light.position.set( 1, 1, 1 ).normalize();
  camera.rotation.y = Math.sin(frameCount*0.0001)*Math.PI*2.0;
  frameCount++;

  for(var i = 0; i < meshArray.length; i++) {
	  meshArray[i].rotation.x += (frameCount*0.01 + Math.sin(((Math.PI*0.05)) + i * Math.PI * 2 / 20.0) * 4.0) * 0.001;
	  meshArray[i].rotation.y -= camera.rotation.y*0.01;
  }

  if (vrMode) camera.quaternion.copy(
  	frame.orientation
  );
}

function render(renderTime, vrInput) {

  requestAnimationFrame( render );


  if (typeof(vrInput) !== 'object' || vrInput.eye === 0) {
    animate(vrInput);
  }

  renderer.render(scene, camera);
}

if (!vrMode) {
  var container = document.createElement( 'div' );
  document.body.appendChild( container );
  container.appendChild( renderer.domElement );
}

render();