// /*
//  * Sample RenderHooks Application
//  */
//
// let vrMode = (typeof renderhooks === 'object');
// if (typeof THREE === 'undefined') { var THREE = require('three'); }
// const scene = new THREE.Scene();
// let camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
// let renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true});
// renderer.setSize( window.innerWidth, window.innerHeight );
// renderer.setClearColor( "rgb(10, 10, 10)", 1 );
//
// let light = new THREE.DirectionalLight("rgb(255, 255, 255)");
// light.position.set( 10, 1, 1 ).normalize();
//
// let geometry = new THREE.Geometry();
// //generateGrid();
//
// //noinspection JSDuplicatedDeclaration
// let material = new THREE.LineBasicMaterial({color: "rgb(255, 255, 255)", opacity: 0.5, transparent: true});
// let line = new THREE.Line(geometry, material, THREE.LinePieces);
// line.position.y = -10;
// scene.add( line );
//
// //noinspection JSDuplicatedDeclaration
// geometry = new THREE.SphereGeometry(1, 20, 64, 0, Math.PI * 2, 0, Math.PI * 2);
// //noinspection JSDuplicatedDeclaration
// material = new THREE.MeshBasicMaterial({color: "rgb(255, 100, 0)"});
//
// const cube = new THREE.Mesh(geometry, material);
// cube.position.x = 0;
// cube.position.y = -10;
// cube.position.z = 0;
// scene.add(cube);
//
// let index = 0;
//
//
//
// scene.add( light );
// camera.position.z = 100;
//
// var frameCount = 0.0;
//
// function animate(frame) {
//
//   //camera.position.z = 50 + ( 20*(Math.sin(frameCount*0.05)*Math.PI*2.0) );
// 	//camera.rotation.x = -Math.PI*0.2;
// 	//camera.rotation.z = ((Math.sin(frameCount*0.0005)*Math.PI*2.0) );
//
// 	//camera.position.z += 0.1;
// //	50 + ( 20*(Math.sin(frameCount*0.00005)*Math.PI*2.0) );
//
//
// 	frameCount++;
//
//   if (vrMode) camera.quaternion.copy(
//   	frame.orientation
//   );
// }
//
// function render(renderTime, vrInput) {
//
//   requestAnimationFrame( render );
//
//
//   if (typeof(vrInput) !== 'object' || vrInput.eye === 0) {
//     animate(vrInput);
//   }
//
//   renderer.render(scene, camera);
// }
//
// if (!vrMode) {
//   var container = document.createElement( 'div' );
//   document.body.appendChild( container );
//   container.appendChild( renderer.domElement );
// }
//
// render();