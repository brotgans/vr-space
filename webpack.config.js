const path = require('path');
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const express = require('express');


const app = express();

app.set('port', (process.env.PORT || 3030));

module.exports = {
	entry: './src',
	output: {
		path: 'build',
		filename: 'bundle.js',
		publicPath: '/',
	},
	module: {
		preLoaders: [

		],
		loaders: [
			{
				test: /\.json$/,
				exclude: /node_modules/,
				loader: 'json-loader'
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					cacheDirectory: true,
					presets: ['react-hmre', 'react', 'es2015']
				}
			},
			{
				test: /\.scss$/,
				loaders: [
					'style',
					'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
					'resolve-url',
					'sass?sourceMap']
			},
			{
				test: /\.css$/,
				loader: 'style!css',
			},
			{
				test: /\.html/,
				loader: 'html',
			},
			{
				test: /\.(png|jpg|pdf)$/,
				loader: 'url-loader?limit=8192'
			},
			{
				test: /\.(gif|svg)$/,
				loaders: [
					'file?hash=sha512&digest=hex&name=[hash].[ext]',
					'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
				]
			}

		],
	},
	devtool: 'cheap-source-map',
	devServer: {
		port: app.get('port'),
		host: '0.0.0.0',

		// entry: path.join(__dirname, 'src'),
		contentBase: path.join(__dirname, 'src'),

		// Enable history API fallback so HTML5 History API based
		// routing works. This is a good default that will come
		// in handy in more complicated setups.
		historyApiFallback: true,
		hot: true,
		inline: true,
		progress: true,
		open: true,

		// Display only errors to reduce the amount of output.
		stats: 'errors-only',
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new CopyWebpackPlugin([{ from: './src/index.html' }])
	]
};
